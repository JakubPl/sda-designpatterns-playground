package pl.sda.designpatters.model;

import pl.sda.designpatters.Size;

public class Cat implements Greatable{
    private String name;
    private Size size;

    public String greeting() {
        return "miau miau!";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}

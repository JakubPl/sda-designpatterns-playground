package pl.sda.designpatters.model;

import pl.sda.designpatters.Color;

public class Person implements Greatable{
    private final String name;
    private final String surname;
    private final Integer age;
    private final Color hairColor;


    private Person(PersonBuilder builder) {
        this.age = builder.age;
        this.name = builder.name;
        this.surname = builder.surname;
        this.hairColor = builder.hairColor;
    }

    /*private Person(String name, String surname, Integer age, Color hairColor) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.hairColor = hairColor;
    }*/

    public String greeting() {
        return "Czesc!";
    }

    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }


    public Color getHairColor() {
        return hairColor;
    }

    public Integer getAge() {
        return age;
    }

    public static PersonBuilder builder() {
        return new PersonBuilder();
    }

    public static class PersonBuilder {
        private Color hairColor;
        private String name;
        private String surname;
        private Integer age;

        public PersonBuilder hairColor(Color hairColor) {
            this.hairColor = hairColor;
            return this;
        }

        public PersonBuilder age(Integer age) {
            this.age = age;
            return this;
        }

        public PersonBuilder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public PersonBuilder name(String name) {
            this.name = name;
            return this;
        }

        public Person build() {
            return new Person(this);
        }
    }
}

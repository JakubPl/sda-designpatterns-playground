package pl.sda.designpatters.model;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Composite implements Greatable {
    private List<Greatable> greatableList = new LinkedList<Greatable>();

    public Composite(Greatable... args) {
        if(args != null) {
            for(Greatable greatable: args) {
                this.greatableList.add(greatable);
            }
        }
    }

    public void addGreatable(Greatable greatable) {
        this.greatableList.add(greatable);
    }

    public void setGreatableList(List<Greatable> greatableList) {
        this.greatableList = greatableList;
    }

    public String greeting() {
        String greetings = greatableList.stream()
                .map(gretable -> gretable.greeting())
                .collect(Collectors.joining(" "));
        return greetings;
    }
}

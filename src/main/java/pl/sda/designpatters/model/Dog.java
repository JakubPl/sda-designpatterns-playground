package pl.sda.designpatters.model;

public class Dog implements Greatable{
    private String name;
    private String type;

    public String greeting() {
        return "hau hau!";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

package pl.sda.designpatters;

import pl.sda.designpatters.model.*;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setName("Horacy");
        cat.setSize(Size.L);

        Dog dog = new Dog();
        dog.setName("Reksio");
        dog.setType("Uliczny");


        //Person.PersonBuilder personBuilder = new Person.PersonBuilder();
        Person.PersonBuilder builder = Person.builder();
        Person person = builder.age(10)
                .hairColor(Color.BLACK)
                .name("Jan")
                .surname("Kowalski")
                .build();

        List<Greatable> greatableList = new LinkedList<Greatable>();

        greatableList.add(person);
        greatableList.add(cat);
        greatableList.add(dog);


        Composite composite1 = new Composite(person, cat, dog);
        Composite composite2 = new Composite(person, cat, dog);


        Composite composite3 =
                new Composite(composite1, composite2);

        /*greatableComposite.addGreatable(person);
        greatableComposite.addGreatable(cat);
        greatableComposite.addGreatable(dog);*/


        greatableList.add(composite2);
        greatableList.add(composite3);

        for (Greatable greatable : greatableList) {
            System.out.print(greatable.greeting() + "\n");
        }

        //System.out.println(greatableComposite.greeting());
    }
}
